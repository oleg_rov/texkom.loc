<?include 'paths.php'?>
<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700&display=swap&subset=cyrillic" rel="stylesheet">
	<link rel="stylesheet" href="local/templates/tech_landing/build/css/main.min.css">
	<script src="local/templates/tech_landing/build/js/script.min.js" async></script>
</head>
<body>
<div class="wrapper">
	<div class="cookie-message">
		<div class="container">
			<h4 class="cookie-message__title">
				Использование cookie
			</h4>
			<div class="cookie-message__content">
				<input type="checkbox" id="cookie-ok" class="cookie-message__ok">
				<p class="cookie-message__message">
				Мы используем файлы cookies для того, чтобы улучшить работу нашего сайта. Продолжая использовать веб-сайт, вы соглашаетесь на использование нами файлов cookies. Если Вы не согласны просим изменить соответствующие настройки браузера.
				</p>
			</div>
			<button class="cookie-message__btn">Принять</button>
		</div>
	</div>

	<header class="header">
		<div class="header-top">
			<div class="container">
				<div class="header-logo">
					<div class="header-logo__main">
						<img src="<?=IMGS_DIR?>logo.svg" alt="техком">
					</div>
					<div class="header-logo__ext">
						<div class="header-logo__name">
							<img src="<?=IMGS_DIR?>logo_name.svg" alt="техком">
						</div>
						<div class="header-logo__slogan">
							Мы умеем удивлять!
						</div>
					</div>
				</div>
				<div class="header-brand-logo">
					<img src="<?=IMGS_DIR?>logo_mitsubishi.svg" alt="mitsubishi">
				</div>
			</div>
		</div>

		<div class="header-banner">
			<div class="header-banner__bg">
				<picture class="header-banner__car">
					<source srcset="<?=IMGS_DIR?>car_m_01.png" media="(max-width: 600px)">
					<img src="<?=IMGS_DIR?>car_01.png" alt="mitsubishi">
				</picture>
				<picture class="header-banner__car">
					<source srcset="<?=IMGS_DIR?>car_m_02.png" media="(max-width: 600px)">
					<img src="<?=IMGS_DIR?>car_02.png" alt="mitsubishi">
				</picture>
			</div>
		</div>

		<div class="header-bottom">
			<div class="container">
				<p class="header-blockquote">
				Качественные запчасти — залог безопасности Вашего автомобиля!
				</p>
			</div>
		</div>

	</header>

	<div class="content">
		<section class="section-first">
			<div class="container">
				<div class="section-first__txt">
					<p>
						Каталог подбора оригинальных автозапчастей для автомобилей Mitsubishi L200, Outlander и Pajero sport по VIN-коду. Доступные цены!<br>
						Заказывая у нас, Вы получаете оригиальные запчасти без предоплаты с доставкой.
					</p>
					<p>
						Попробуйте поискать по VIN или просто позвоните нашему специалисту и убедитесь, насколько наша цена оправдает ваше ожидание!
					</p>
				</div>
				<h2 class="section-first__header">
					Заказывая автозапчасти у нас, Вы получите следующие преимущества:
				</h2>
				<div class="section-first__list">
					<ul class="section-first__list-part">
						<li>Без предоплат</li>
						<li>Быстрая доставка вашего заказа</li>
						<li>Гарантия наличия</li>
					</ul>
					<ul class="section-first__list-part">
						<li> Возврат без проблем</li>
						<li>Подскажем по заменам номера</li>
						<li>Оригинальное происхождение</li>
					</ul>
				</div>
			</div>
		</section>

		<section class="section-second">
			<div class="forms-outer-container">
				<h2 class="section-second__header">
					Цены на наши оригинальные запчасти гораздо ниже рыночных.
				</h2>
				<p class="section-second__txt">
					В этом очень просто убедится: попробуйте поискать по VIN  вашего авто или просто оставьте номер вашего телефона. <br>Наш специалист проконсультирует и подберет все необходимые для ремонта детали.
				</p>
				<div class="forms-inner-container">
					<div class="search-vim-form">
						<h3 class="search-vim-form__title forms__title">
							Найдите модель по VIN-номеру
						</h3>
						<form id="search-vim-form">
							<div class="form-search-group">
								<input type="text" class="search-vim-input">
								<button type="submit" class="search-vim-submit">
									<svg>
						                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#search"></use>
						            </svg>
								</button>
							</div>
						</form>
						<p class="search-vim-sample">
							Например: <span>WAUBH54B11N111054</span>
						</p>
						<p class="search-vim-txt">
							VIN вашего автомобиля является самым надежным идентификатором. <br>Если Вы не знаете VIN или сомневаетесь, подойдет ли выбранная деталь к вашему автомобилю - свяжитесь с Вашим менеджером
						</p>
					</div>
					<div class="forms-delimiter">
						<span>или</span>
					</div>
					<div class="call-us-form">
						<h3 class="call-us-form__title forms__title">
							Отправить запрос специалисту
						</h3>
						<form id="call-us-form">
							<div class="form-call-us-group">
								<input type="text" class="call-us-input" placeholder="Телефон">
								<button type="submit" class="call-us-submit a-style-btn">
								ЖДУ ЗВОНКА
								</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</section>

		<section class="section-third">
			<div class="section-third__bg"></div>

			<a href="https://texkom.ru/" target="_blank">
				<div class="bottom-logo">
					<div class="bottom-logo__main">
						<img src="<?=IMGS_DIR?>logo.svg" alt="техком">
					</div>
					<div class="bottom-logo__ext">
						<div class="bottom-logo__name">
							<img src="<?=IMGS_DIR?>logo_name.svg" alt="техком">
						</div>
						<div class="bottom-logo__slogan">
							Мы умеем удивлять!
						</div>
					</div>
				</div>
			</a>

			<div class="requisites-block">
				<h3 class="requisites-block__tile">
				Остались вопросы?
				</h3>
				<p class="requisites-block__info">
					<span class="requisites-block__phone">
						<a href="tel:84953200909">8 (495) 320-09-09</a>
					</span>
					<span class="requisites-block__hours">
						<svg>
			                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#clock"></use>
			            </svg>
			            24/7
					</span>
				</p>
				<p class="requisites-block__dep-name">
					отдел иномарок
				</p>
			</div>

			<div class="social-block">
				<h3 class="social-block__tile">
					Мы в социальных сетях:
				</h3>

				<div class="social-block__items">
					<a href="https://www.youtube.com/" class="social-block__item" target="_blank">
						<svg class="yt-link">
			                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#yt"></use>
			            </svg>
					</a>

					<a href="https://ok.ru" class="social-block__item" target="_blank">
						<svg class="oks-link">
			                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#oks"></use>
			            </svg>
					</a>

					<a href="https://www.facebook.com" class="social-block__item" target="_blank">
						<svg class="fb-link">
			                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#fb"></use>
			            </svg>
					</a>

					<a href="https://vk.com" class="social-block__item" target="_blank">
						<svg class="vk-link">
			                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#vk"></use>
			            </svg>
					</a>
				</div>
			</div>
		</section>
	</div>

	<footer class="footer">
		<div class="container">
			<div class="footer-block">
			© 2019 Texkom. Все права защищены
			</div>
			<div class="footer-block">
			ООО «АВТОМАГ» Адрес: 117405 Г. Москва, Варшавское шоссе 170 Г
			</div>
		</div>
	</footer>

	<div class="modal-ok">
		<div class="modal-ok__bg"></div>
		<div class="modal-ok__window">
			<span class="modal-ok__close">
				<svg>
	                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#close"></use>
	            </svg>
			</span>
			<div class="modal-ok__content">
				Ваш запрос отправлен.<br> Мы свяжемся с вами в ближайшее время.
			</div>
		</div>
	</div>

	<div class="page-preloader">
		<div class="page-preloader__spinner wait-spin">
			<svg>
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?=IMGS_DIR?>sprites.svg#spinner"></use>
            </svg>
		</div>
	</div>

</div>
</body>
</html>