window.addEventListener('load', function() {

	// search vim form
	$('.search-vim-input').val('');
	$('#search-vim-form').on('submit', function(e) {
		e.preventDefault();
		var input = $('.search-vim-input');
		var query = input.val();
		query = query.trim();

		if (query != '') {
			$('.page-preloader').show();
			location.href = 'https://texkom.ru/originalnyie-katalogi/?vin=' + query;
		} else {
			input.css('outline', '1px solid red');
		}
	});
	// /search vim form

	// call us form
	$('#call-us-form').on('submit', function(e) {
		e.preventDefault();
		var form = $(this);
		var input = $('.call-us-input');
		var tel = input.val();

		if (tel != '') {
			input.css('outline', '');

			$.ajax({
				method: "POST",
				url: '/local/templates/tech_landing/call_me.php',
				data: { phone : tel },
				beforeSend: function() {
					$('.page-preloader').show();
				},
				success: function(res) {
					$('.page-preloader').delay(500).fadeOut(function() {
						$('.modal-ok').fadeIn();
						$('.modal-ok__content').html(res);
						input.val('');
					});
				},
			});

		} else {
			input.css('outline', '1px solid red');
		}
	});

	$(".call-us-input").inputmask("8 (999) 999-99-99");
	// /call us form

	// cookie message
	$('.cookie-message__btn').click(function() {
		var parent = $(this).closest('.cookie-message');
		var checkbox = parent.find('.cookie-message__ok');

		if (checkbox.prop("checked")) {
			parent.slideUp(function() {
				$('.header').css('margin-top', '');
				$.cookie('cookie-agree', 'y');
			});
		} else {
			checkbox.css('outline', '1px solid red');
		}

	});

	function showMess() {
		if ($.cookie('cookie-agree') == 'y') {
			return;
		}
		var mess = $('.cookie-message');
		var messHeight = mess.outerHeight();
		var header = $('.header');
		mess.css({
			'display': 'block',
			'position': 'fixed',
			'z-index': '10'
		});
		header.css('margin-top', messHeight + 'px');
	}

	showMess();
	$(window).resize(function() {
		var mess = $('.cookie-message');
		if(mess.is(":visible"))
			showMess();

	});
	// /cookie message

	// ok modal window
	$(document).click(function(event) {
		if ($(event.target).closest('.modal-ok__window').length ||
			$(event.target).closest('.call-us-submit').length) return;
	    $('.modal-ok').fadeOut();
	    event.stopPropagation();
	});
	$('.modal-ok__close').click(function() {
		 $('.modal-ok').fadeOut();
	});
	// /ok modal window

});